/*
 * This is the source code of DMAudioStreaming for Android v. 1.0.0.
 * You should have received a copy of the license in this archive (see LICENSE).
 * Copyright @Dibakar_Mistry(dibakar.ece@gmail.com), 2017.
 */
package com.newitventure.audiostreamingapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;

import com.newitventure.audiostreamingapp.R;
import com.newitventure.audiostreamingapp.network.MusicBrowser;
import com.newitventure.audiostreamingapp.network.MusicLoaderListener;
import com.newitventure.lib.music.MediaMetaData;
import com.newitventure.lib.music.activity.MusicPlayingActivity;
import com.newitventure.lib.music.activity.ViewManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewManager viewManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViewManager();
        loadMusicData();

    }

    private void initializeViewManager() {
        viewManager = ViewManager.getInstance();
        viewManager.setRadioType(false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void loadMusicData() {
        MusicBrowser.loadMusic(this, new MusicLoaderListener() {
            @Override
            public void onLoadSuccess(List<MediaMetaData> listMusic) {
                Intent intent = new Intent(MainActivity.this, MusicPlayingActivity.class);
                intent.putParcelableArrayListExtra("musicList", (ArrayList<? extends Parcelable>) listMusic);
                startActivity(intent);
                finish();
            }

            @Override
            public void onLoadFailed() {
                //TODO SHOW FAILED REASON
            }

            @Override
            public void onLoadError() {
                //TODO SHOW ERROR
            }
        });
    }
}