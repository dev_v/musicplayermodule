/*
 * This is the source code of DMAudioStreaming for Android v. 1.0.0.
 * You should have received a copy of the license in this archive (see LICENSE).
 * Copyright @Dibakar_Mistry(dibakar.ece@gmail.com), 2017.
 */
package com.newitventure.lib.music.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.newitventure.lib.music.MediaMetaData;
import com.newitventure.lib.music.R;
import com.newitventure.lib.music.widgets.MusicEqualizerBarView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterMusic extends RecyclerView.Adapter<AdapterMusic.MeroViewHolder> {
    private List<MediaMetaData> musicList;
    private Context mContext;

    public AdapterMusic(Context context, List<MediaMetaData> audioList) {
        this.musicList = audioList;
        this.mContext = context;
    }

//    public void refresh(List<MediaMetaData> musicList) {
//        if (this.musicList != null) {
//            this.musicList.clear();
//        }
//        this.musicList.addAll(musicList);
//        notifyDataSetChanged();
//    }
//
    public void notifyPlayState(MediaMetaData metaData) {
        if (this.musicList != null && metaData != null) {
            int index = this.musicList.indexOf(metaData);
            //TODO SOMETIME INDEX RETURN -1 THOUGH THE OBJECT PRESENT IN THIS LIST
            if (index == -1) {
                for (int i = 0; i < this.musicList.size(); i++) {
                    if (this.musicList.get(i).getMediaId().equalsIgnoreCase(metaData.getMediaId())) {
                        index = i;
                        break;
                    }
                }
            }
            if (index > 0 && index < this.musicList.size()) {
                this.musicList.set(index, metaData);
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.listview_single_item, parent, false);
        return new MeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeroViewHolder mViewHolder, final int position) {
        MediaMetaData media = musicList.get(position);

        mViewHolder.mediaTitle.setText(media.getMediaTitle());
        mViewHolder.MediaDesc.setText(media.getMediaArtist());
        Picasso.with(mContext).load(media.getMediaArt()).placeholder(R.drawable.bg_default_album_art).into(mViewHolder.mediaArt);

        switch (media.getPlayState()) {

            case PlaybackStateCompat.STATE_NONE:
                mViewHolder.progressBar.setVisibility(View.GONE);
                mViewHolder.playButton.setVisibility(View.VISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.INVISIBLE);
                mViewHolder.equalizerBarView.stopBars();
                break;

            case PlaybackStateCompat.STATE_PLAYING:
                mViewHolder.progressBar.setVisibility(View.GONE);
                mViewHolder.playButton.setVisibility(View.INVISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.VISIBLE);
                mViewHolder.equalizerBarView.animateBars();
                break;

            case PlaybackStateCompat.STATE_PAUSED:
                mViewHolder.playButton.setVisibility(View.INVISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.VISIBLE);
                mViewHolder.equalizerBarView.stopBars();
                break;

            case PlaybackStateCompat.STATE_STOPPED:
                mViewHolder.progressBar.setVisibility(View.GONE);
                mViewHolder.playButton.setVisibility(View.VISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.INVISIBLE);
                break;

            case PlaybackStateCompat.STATE_BUFFERING:
                mViewHolder.progressBar.setVisibility(View.VISIBLE);
                mViewHolder.playButton.setVisibility(View.INVISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.INVISIBLE);
                break;

            default:
                mViewHolder.progressBar.setVisibility(View.GONE);
                mViewHolder.playButton.setVisibility(View.VISIBLE);
                mViewHolder.equalizerBarView.setVisibility(View.INVISIBLE);
                mViewHolder.equalizerBarView.stopBars();
                break;
        }

        mViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listItemListener != null) {
                    listItemListener.onItemClickListener(musicList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    public class MeroViewHolder extends RecyclerView.ViewHolder {
        public ImageView mediaArt;
        public ImageView playButton;
        public TextView mediaTitle;
        public TextView MediaDesc;
        MusicEqualizerBarView equalizerBarView;
        ProgressBar progressBar;

        public MeroViewHolder(View itemView) {
            super(itemView);
            mediaArt = itemView.findViewById(R.id.img_mediaArt);
            playButton = itemView.findViewById(R.id.img_play);
            mediaTitle = itemView.findViewById(R.id.text_mediaTitle);
            MediaDesc = itemView.findViewById(R.id.text_mediaDesc);
            equalizerBarView = itemView.findViewById(R.id.music_equalizer);
            progressBar = itemView.findViewById(R.id.progress_bar);

        }
    }

    public void setListItemListener(ListItemListener listItemListener) {
        this.listItemListener = listItemListener;
    }

    public ListItemListener listItemListener;

    public interface ListItemListener {
        void onItemClickListener(MediaMetaData media);
    }
}
