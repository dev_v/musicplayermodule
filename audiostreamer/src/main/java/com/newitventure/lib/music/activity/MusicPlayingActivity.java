/*
 * This is the source code of DMAudioStreaming for Android v. 1.0.0.
 * You should have received a copy of the license in this archive (see LICENSE).
 * Copyright @Dibakar_Mistry(dibakar.ece@gmail.com), 2017.
 */
package com.newitventure.lib.music.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.newitventure.lib.music.MediaMetaData;
import com.newitventure.lib.music.R;
import com.newitventure.lib.music.adapter.AdapterMusic;
import com.newitventure.lib.music.slideup.SlidingUpPanelLayout;
import com.newitventure.lib.music.streaming.AudioStreamingManager;
import com.newitventure.lib.music.streaming.CurrentSessionCallback;
import com.newitventure.lib.music.widgets.BlurImage;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class MusicPlayingActivity extends AppCompatActivity implements CurrentSessionCallback, View.OnClickListener {

    private Context context;
    private RecyclerView musicList;
    private AdapterMusic adapterMusic;

    private SlidingUpPanelLayout slidingUpPanelLayout;
    private boolean isExpand = false;

    LinearLayout fullPlayerLayout, seekBarLayout;
    RelativeLayout wholePlayerLayout, miniPlayerLayout, miniPlayButtonLayout, fullPlayButtonLayout;
    TextView audioTitle, audioOwner, audioMaxLength, audioProgress, fullPlayerTitle, fullPlayerOwner;
    ImageView playerAudioImage, fullAudioImage, backgroundImage;
    SeekBar seekBar, fullSeekBar;
    ProgressBar playerProgressBar, fullPlayerProgressBar;
    ImageButton previousButton, nextButton, repeatButton, shuffleButton, miniNextButton, miniPreviousButton, miniPlayButton, miniPauseButton, fullPlayButton, pauseButton;
    boolean isShuffle = false, isRepeat = false;

    //For  Playing part implementation
    private AudioStreamingManager streamingManager;
    private MediaMetaData currentSong;
    private List<MediaMetaData> listOfSongs = new ArrayList<>();
    private boolean typeRadio = false; //to check if it is for radio or music

    //For changing player's button
    int playButtonId, previousButtonId, nextButtonId;

    //for blur image background
    Target target;
    private int songDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        this.context = MusicPlayingActivity.this;
        listOfSongs = getIntent().getParcelableArrayListExtra("musicList");
        typeRadio = ViewManager.getInstance().isRadioType();

        configAudioStreamer();
        uiInitialization();
        checkAlreadyPlaying();
    }



    @Override
    public void onBackPressed() {
        if (isExpand) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
            overridePendingTransition(0, 0);
            finish();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            if (streamingManager != null) {
                streamingManager.subscribesCallBack(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        try {
            if (streamingManager != null) {
                streamingManager.unSubscribeCallBack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            if (streamingManager != null) {
                streamingManager.unSubscribeCallBack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void updatePlaybackState(int state) {
        Log.e("updatePlaybackState: ", "" + state);
        switch (state) {
            case PlaybackStateCompat.STATE_PLAYING:
                playerProgressBar.setVisibility(View.GONE);
                fullPlayerProgressBar.setVisibility(View.GONE);
                miniPlayButton.setVisibility(View.GONE);
                fullPlayButton.setVisibility(View.GONE);
                miniPauseButton.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.VISIBLE);
                if (currentSong != null) {
                    currentSong.setPlayState(PlaybackStateCompat.STATE_PLAYING);
                    notifyAdapter(currentSong);
                }
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                miniPlayButton.setVisibility(View.VISIBLE);
                fullPlayButton.setVisibility(View.VISIBLE);
                pauseButton.setVisibility(View.GONE);
                miniPauseButton.setVisibility(View.GONE);
                if (currentSong != null) {
                    currentSong.setPlayState(PlaybackStateCompat.STATE_PAUSED);
                    notifyAdapter(currentSong);
                }
                break;
            case PlaybackStateCompat.STATE_NONE:
                currentSong.setPlayState(PlaybackStateCompat.STATE_NONE);
                notifyAdapter(currentSong);
                break;
            case PlaybackStateCompat.STATE_STOPPED:
                miniPlayButton.setVisibility(View.VISIBLE);
                fullPlayButton.setVisibility(View.VISIBLE);
                miniPauseButton.setVisibility(View.GONE);
                pauseButton.setVisibility(View.GONE);
                if (currentSong != null) {
                    currentSong.setPlayState(PlaybackStateCompat.STATE_NONE);
                    notifyAdapter(currentSong);
                }
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                playerProgressBar.setVisibility(View.VISIBLE);
                fullPlayerProgressBar.setVisibility(View.VISIBLE);
                miniPlayButton.setVisibility(View.INVISIBLE);
                fullPlayButton.setVisibility(View.INVISIBLE);
                miniPauseButton.setVisibility(View.INVISIBLE);
                pauseButton.setVisibility(View.INVISIBLE);
                if (currentSong != null) {
                    currentSong.setPlayState(PlaybackStateCompat.STATE_NONE);
                    notifyAdapter(currentSong);
                }
                break;
        }
    }

    @Override
    public void playSongComplete() {
        streamingManager.onSeekTo(0);
        streamingManager.scheduleSeekBarUpdate();
        miniPlayButton.setVisibility(View.VISIBLE);
        fullPlayButton.setVisibility(View.VISIBLE);
        miniPauseButton.setVisibility(View.GONE);
        pauseButton.setVisibility(View.GONE);
    }

    @Override
    public void currentSeekBarPosition(int progress) {
        setUpSeekBar(seekBar, progress);
        setUpSeekBar(fullSeekBar, progress);
    }

    @Override
    public void playCurrent(int indexP, MediaMetaData currentAudio) {
        showMediaInfo(currentAudio);
        notifyAdapter(currentAudio);
    }

    @Override
    public void playNext(int indexP, MediaMetaData CurrentAudio) {
        showMediaInfo(CurrentAudio);
    }

    @Override
    public void playPrevious(int indexP, MediaMetaData currentAudio) {
        showMediaInfo(currentAudio);
    }

    @Override
    public void onClick(View view) {
        view.getId();
            if ((view.getId() == R.id.next_btn) || (view.getId() == R.id.mini_next)) {
                streamingManager.onSkipToNext();
            }
            if ((view.getId() == R.id.previous_btn) || (view.getId() == R.id.mini_previous)) {
                streamingManager.onSkipToPrevious();
            }
            if ((view.getId() == R.id.play_btn_layout) || (view.getId() == R.id.mini_play_btn_layout)) {
                if (currentSong != null) {
                    playPauseEvent(view);
                }
            }
            if (view.getId() == R.id.repeat_btn) {
                if (isRepeat) {
                    isRepeat = false;
                    repeatButton.setColorFilter(getResources().getColor(R.color.md_text_white));
                } else {
                    isRepeat = true;
                    isShuffle = false;
                    repeatButton.setColorFilter(Color.parseColor("#fd7f47"));
                    shuffleButton.setColorFilter(getResources().getColor(R.color.md_text_white));
                }
                streamingManager.onRepeat(isRepeat);
            }
            if (view.getId() == R.id.shuffle_btn) {

                if (isShuffle) {
                    isShuffle = false;
                    shuffleButton.setColorFilter(getResources().getColor(R.color.md_text_white));
                } else {
                    isShuffle = true;
                    isRepeat = false;
                    shuffleButton.setColorFilter(Color.parseColor("#fd7f47"));
                    repeatButton.setColorFilter(getResources().getColor(R.color.md_text_white));
                }
                streamingManager.onShuffle(isShuffle);
            }
    }

    private void notifyAdapter(MediaMetaData media) {
        adapterMusic.notifyPlayState(media);
    }

    private void playPauseEvent(View v) {
        if (streamingManager.isPlaying()) {
            streamingManager.onMusicPause();
            miniPlayButton.setVisibility(View.VISIBLE);
            fullPlayButton.setVisibility(View.VISIBLE);
            miniPauseButton.setVisibility(View.GONE);
            pauseButton.setVisibility(View.GONE);
        } else {
            streamingManager.onPlay(currentSong);
            miniPlayButton.setVisibility(View.GONE);
            fullPlayButton.setVisibility(View.GONE);
            miniPauseButton.setVisibility(View.VISIBLE);
            pauseButton.setVisibility(View.VISIBLE);

        }
    }

    private void playSong(MediaMetaData media) {
        if (streamingManager != null) {
            streamingManager.onPlay(media);
            showMediaInfo(media);
        }
    }

    private void showMediaInfo(MediaMetaData media) {
        currentSong = media;
        songDuration = Integer.valueOf(media.getMediaDuration()) * 1000;
        seekBar.setProgress(0);
        fullSeekBar.setProgress(0);
        seekBar.setMax(songDuration);
        fullSeekBar.setMax(songDuration);

        loadSongDetails(media);
    }

    private void configAudioStreamer() {
        streamingManager = AudioStreamingManager.getInstance(context);
        //Set PlayMultiple 'true' if want to playing sequentially one by one songs
        // and provide the list of songs else set it 'false'
        streamingManager.setPlayMultiple(true);
        streamingManager.setMediaList(listOfSongs);
        //If you want to show the Player Notification then set ShowPlayerNotification as true
        //and provide the pending intent so that after click on notification it will redirect to an activity
        streamingManager.setShowPlayerNotification(true);
        streamingManager.setPendingIntentAct(getNotificationPendingIntent());
    }


    private void uiInitialization() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));

        slidingUpPanelLayout = findViewById(R.id.sliding_layout);
        wholePlayerLayout = findViewById(R.id.include_sliding_panel_childtwo);
        backgroundImage = wholePlayerLayout.findViewById(R.id.background_image);
        fullPlayerTitle = wholePlayerLayout.findViewById(R.id.audio_title);
        fullPlayerOwner = wholePlayerLayout.findViewById(R.id.audio_owner);
        miniPlayButtonLayout = wholePlayerLayout.findViewById(R.id.mini_play_btn_layout);
        fullPlayButtonLayout = wholePlayerLayout.findViewById(R.id.play_btn_layout);
        miniPlayButton = wholePlayerLayout.findViewById(R.id.audioPlay);
        fullPlayButton = wholePlayerLayout.findViewById(R.id.play_btn);
        miniPauseButton = wholePlayerLayout.findViewById(R.id.audioPause);
        pauseButton = wholePlayerLayout.findViewById(R.id.pause_btn);
        audioMaxLength = wholePlayerLayout.findViewById(R.id.audio_length);
        audioProgress = wholePlayerLayout.findViewById(R.id.audio_progress);
        miniPlayerLayout = wholePlayerLayout.findViewById(R.id.mini_player_layout);
        audioTitle = wholePlayerLayout.findViewById(R.id.audioTitle);
        audioOwner = wholePlayerLayout.findViewById(R.id.audioOwner);
        seekBarLayout = wholePlayerLayout.findViewById(R.id.seekbar_layout);
        seekBar = wholePlayerLayout.findViewById(R.id.seekBar);
        playerAudioImage = wholePlayerLayout.findViewById(R.id.audio_image);
        playerProgressBar = wholePlayerLayout.findViewById(R.id.player_progress_bar);
        fullPlayerLayout = wholePlayerLayout.findViewById(R.id.full_player_layout);
        fullAudioImage = wholePlayerLayout.findViewById(R.id.full_audio_image);
        previousButton = wholePlayerLayout.findViewById(R.id.previous_btn);
        miniNextButton = wholePlayerLayout.findViewById(R.id.mini_next);
        miniPreviousButton = wholePlayerLayout.findViewById(R.id.mini_previous);
        nextButton = wholePlayerLayout.findViewById(R.id.next_btn);
        repeatButton = wholePlayerLayout.findViewById(R.id.repeat_btn);
        shuffleButton = wholePlayerLayout.findViewById(R.id.shuffle_btn);
        fullSeekBar = wholePlayerLayout.findViewById(R.id.full_audio_seekBar);
        fullPlayerProgressBar = wholePlayerLayout.findViewById(R.id.full_player_progress_bar);

        if (this.typeRadio) {
            repeatButton.setVisibility(View.GONE);
            shuffleButton.setVisibility(View.GONE);
            seekBar.setVisibility(View.GONE);
            seekBarLayout.setVisibility(View.GONE);
        }

//        if (playButtonId != 0) {
//            miniPlayButton.setImageResource(playButtonId);
//            fullPlayButton.setImageResource(playButtonId);
//        }
//        if (previousButtonId != 0) {
//            miniPreviousButton.setImageResource(previousButtonId);
//            previousButton.setImageResource(previousButtonId);
//        }
//        if (nextButtonId != 0) {
//            miniNextButton.setImageResource(nextButtonId);
//            nextButton.setImageResource(nextButtonId);
//        }

        fullPlayButtonLayout.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        previousButton.setOnClickListener(this);
        miniPlayButtonLayout.setOnClickListener(this);
        miniNextButton.setOnClickListener(this);
        miniPreviousButton.setOnClickListener(this);
        shuffleButton.setOnClickListener(this);
        repeatButton.setOnClickListener(this);

        musicList = findViewById(R.id.musicList);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

        miniPlayerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
        slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset == 0.0f) {
                    isExpand = false;
                    miniPlayerLayout.animate().alpha(1.0f).setDuration(10).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            miniPlayerLayout.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationResume(Animator animation) {
                            super.onAnimationResume(animation);
                            miniPlayerLayout.setVisibility(View.VISIBLE);
                        }
                    });
                    slidingUpPanelLayout.getChildAt(1).setOnClickListener(null);
                } else if (slideOffset > 0.0f && slideOffset < 1.0f) {
                    //slideBottomView.getBackground().setAlpha((int) slideOffset * 255);
                } else if (slideOffset == 1.0f) {
                    //slideBottomView.getBackground().setAlpha(100);
                    isExpand = true;
                    miniPlayerLayout.animate().alpha(0.0f).setDuration(300).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            miniPlayerLayout.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationResume(Animator animation) {
                            super.onAnimationResume(animation);
                            miniPlayerLayout.setVisibility(View.GONE);
                        }
                    });
//
                }
            }

            @Override
            public void onPanelExpanded(View panel) {
                isExpand = true;
            }

            @Override
            public void onPanelCollapsed(View panel) {
                isExpand = false;
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });

        seekBar.setMax(0);
        fullSeekBar.setMax(0);

        musicList.setHasFixedSize(true);
        musicList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapterMusic = new AdapterMusic(context, listOfSongs);
        musicList.setAdapter(adapterMusic);

        adapterMusic.setListItemListener(new AdapterMusic.ListItemListener() {
            @Override
            public void onItemClickListener(MediaMetaData media) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                playSong(media);
            }
        });

    }

    private void checkAlreadyPlaying() {
        if (streamingManager.isPlaying()) {
            currentSong = streamingManager.getCurrentAudio();
            if (currentSong != null) {
                currentSong.setPlayState(streamingManager.mLastPlaybackState);
                showMediaInfo(currentSong);
//                notifyAdapter(currentSong);
            }
        }
    }

    private void loadSongDetails(MediaMetaData metaData) {
        audioTitle.setText(metaData.getMediaTitle());
        audioOwner.setText(metaData.getMediaArtist());
        fullPlayerTitle.setText(metaData.getMediaTitle());
        fullPlayerOwner.setText(metaData.getMediaArtist());

        Target target = blurImage();
        backgroundImage.setTag(target);
        Picasso.with(context).load(metaData.getMediaArt()).placeholder(R.drawable.bg_default_album_art).into(target);

        Picasso.with(context).load(metaData.getMediaArt()).placeholder(R.drawable.bg_default_album_art).into(playerAudioImage);
        Picasso.with(context).load(metaData.getMediaArt()).placeholder(R.drawable.bg_default_album_art).into(fullAudioImage);

    }

    private Target blurImage() {

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                backgroundImage.setImageBitmap(BlurImage.fastblur(bitmap, 1f, 10));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                backgroundImage.setImageResource(R.drawable.bg_default_album_art);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        return target;
    }

    private PendingIntent getNotificationPendingIntent() {
        Intent intent = new Intent(context, MusicPlayingActivity.class);
        intent.setAction("com.newitventure.lib.music.close");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        return mPendingIntent;
    }

    private void setUpSeekBar(final SeekBar seekBar, final int progress) {

//        Timber.e(TAG, "setUpSeekBar: audio duration==== " + mediaPlayer.getDuration());

        //show audio max time length in 00:00 format
        final int minutes = (songDuration / 1000) / 60;
        final int seconds = ((songDuration / 1000) % 60);
        if (minutes < 10) {
            if (seconds < 10) {
                audioMaxLength.setText("0" + minutes + ":" + "0" + seconds);
            } else audioMaxLength.setText("0" + minutes + ":" + seconds);
        } else if (seconds < 10) {
            audioMaxLength.setText(minutes + ":" + "0" + seconds);
        } else audioMaxLength.setText(minutes + ":" + seconds);
        seekBar.setProgress(progress);
        //show audio progress time in 00:00 format a/c to progress
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                seekBar.setProgress(progress);
//
        final long mMinutes = (progress / 1000) / 60;//converting into minutes
        final long mSeconds = ((progress / 1000) % 60);//converting into seconds
        if (mMinutes < 10) {
            if (mSeconds < 10) {
                audioProgress.setText("0" + mMinutes + ":" + "0" + mSeconds);
            } else audioProgress.setText("0" + mMinutes + ":" + mSeconds);
        } else if (mSeconds < 10) {
            audioProgress.setText(mMinutes + ":" + "0" + mSeconds);
        } else audioProgress.setText(mMinutes + ":" + mSeconds);
//                mHandler.postDelayed(this, 100);
//            }
//        };
//        runnable.run();
//touch listener for seekbar i.e. play da audio a/c to da position of seekbar's thumb
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int position, boolean b) {
                if (b) {
                    streamingManager.onSeekTo(position);
                    streamingManager.scheduleSeekBarUpdate();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

}