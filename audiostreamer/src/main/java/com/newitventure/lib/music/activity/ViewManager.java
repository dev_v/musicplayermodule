package com.newitventure.lib.music.activity;

/**
 * Created by NITV-Vinay on 12/13/2018.
 */

public class ViewManager {
    private static ViewManager instance;
    private int playButtonId, prevButtonId, nextButtonId;
    private boolean radioType;
    private ViewManager(){}

    public static ViewManager getInstance(){

        if (instance == null) {
            instance = new ViewManager();
        }
        return instance;
    }

    public boolean isRadioType() {
        return radioType;
    }

    public void setRadioType(boolean radioType) {
        this.radioType = radioType;
    }

    public int getPlayButtonId() {
        return playButtonId;
    }

    public void setPlayButtonId(int playButtonId) {
        this.playButtonId = playButtonId;
    }

    public int getPrevButtonId() {
        return prevButtonId;
    }

    public void setPrevButtonId(int prevButtonId) {
        this.prevButtonId = prevButtonId;
    }

    public int getNextButtonId() {
        return nextButtonId;
    }

    public void setNextButtonId(int nextButtonId) {
        this.nextButtonId = nextButtonId;
    }

}
