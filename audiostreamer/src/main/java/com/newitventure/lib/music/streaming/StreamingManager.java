package com.newitventure.lib.music.streaming;


import android.support.v7.app.AppCompatActivity;

import com.newitventure.lib.music.MediaMetaData;

public abstract class StreamingManager {

    public abstract void onPlay(MediaMetaData infoData);

    public abstract void onMusicPause();

    public abstract void onMusicStop();

    public abstract void onSeekTo(long position);

    public abstract int lastSeekPosition();

    public abstract void onSkipToNext();

    public abstract void onSkipToPrevious();

    public abstract void onShuffle(boolean isShuffle);

    public abstract void onRepeat(boolean isRepeat);
}
